#pragma once

#include "strengths.hpp"

class Boid {
private:
    glm::vec2 m_position;
    glm::vec2 m_velocity;

    p6::Angle getAngle();
    glm::vec2 getPosition();
    glm::vec2 getVelocity();
    float     distanceBetween(Boid boid); // calcule la distance entre 2 boids
    glm::vec2 alignementRule(std::vector<Boid> boids, strengths strengths);
    glm::vec2 separationRule(std::vector<Boid> boids, strengths strengths);
    glm::vec2 cohesionRule(std::vector<Boid> boids, strengths strengths);

public:
    Boid(glm::vec2 position, glm::vec2 velocity); // constructeur paramétrique
    Boid(const p6::Context& ctx);                 // constructeur boid aléatoire
    ~Boid() = default;                            // destructeur

    void render(p6::Context& ctx);
    void applyRules(std::vector<Boid> boids, strengths strengths);

    void maxSpeed(strengths strengths);
};
