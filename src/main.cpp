#define DOCTEST_CONFIG_IMPLEMENT
#include "doctest/doctest.h"
#include "field.hpp"

float     DISTANCE   = 0.06;
float     vitesseMax = glm::length(glm::vec2(0.007, 0.007));
strengths strengths(1, 1, 1, DISTANCE, vitesseMax);

int main(int argc, char* argv[])
{
    { // Run the tests
        if (doctest::Context{}.run() != 0)
            return EXIT_FAILURE;
        // The CI does not have a GPU so it cannot run the rest of the code.
        const bool no_gpu_available = argc >= 2 && strcmp(argv[1], "-nogpu") == 0; // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        if (no_gpu_available)
            return EXIT_SUCCESS;
    }

    // Actual app
    auto ctx = p6::Context{{.title = "Simple-p6-Setup"}};

    int   n         = 50;            // nombre de boids
    Field fieldtest = Field(n, ctx); // test constructeur field
    ctx.maximize_window();
    ctx.imgui = [&]() {
        // Show a simple window
        ImGui::Begin("Test");
        // ImGui::SliderInt("Number of boids", &n, 0, 100); ///////Ne peut pas marcher puisque le field est créé une fois avant le passage dans la boucle. Demander à Jules si possible de contourner ce pb ?
        ImGui::SliderFloat("distance Between Boids", &DISTANCE, 0.001, 10);
        ImGui::SliderFloat("separation Strength", &(strengths.m_separationStrength), 0, 2);
        ImGui::SliderInt("cohesion Strength", &(strengths.m_cohesionStrength), 0, 10);
        ImGui::SliderInt("alignment Strength", &(strengths.m_alignmentStrength), 0, 10);
        ImGui::SliderFloat("maximum speed", &(strengths.m_vitesseMax), 0.001, 0.02);

        ImGui::End();
        // Show the official ImGui demo window
        // It is very useful to discover all the widgets available in ImGui
    };

    // Declare your infinite update loop.
    ctx.update = [&]() {
        ctx.background({0.151 * 3, 0.253 * 3, 0.135 * 3});
        fieldtest.fieldDraw(ctx);        // test fonction draw
        fieldtest.applyRules(strengths); // test de la applyRules
    };

    // Should be done last. It starts the infinite loop.
    ctx.start();
}
